import machine
import time

# Configuration des broches pour les servomoteurs
servo1_pin = machine.Pin(15)  # Changer le numéro de broche selon le câblage pour le servomoteur 1
servo2_pin = machine.Pin(14)  # Changer le numéro de broche selon le câblage pour le servomoteur 2

servo1_pwm = machine.PWM(servo1_pin)
servo2_pwm = machine.PWM(servo2_pin)

# Configuration des paramètres PWM pour les servomoteurs
servo_freq = 50  # Fréquence de 50 Hz pour les servomoteurs standard
angle_min = 0.5  # Largeur d'impulsion correspondant à l'angle minimum (en ms)
angle_max = 2.5  # Largeur d'impulsion correspondant à l'angle maximum (en ms)

def angle_to_duty_cycle(angle):
    # Convertir l'angle en largeur d'impulsion PWM
    duty_cycle = (angle / 180.0) * (angle_max - angle_min) + angle_min
    return int(duty_cycle / 20 * 65535)  # Conversion en valeur entre 0 et 65535

def rotate_servo(servo_pwm, angle):
    # Convertir l'angle en largeur d'impulsion PWM
    duty_cycle = angle_to_duty_cycle(angle)
    # Appliquer la largeur d'impulsion PWM au servomoteur
    servo_pwm.duty_u16(duty_cycle)
    # Laisser un peu de temps au servomoteur pour se déplacer
    time.sleep(0.1)

# Demander à l'utilisateur de saisir les angles de rotation pour les deux servomoteurs
angle1 = float(input("Entrez l'angle de rotation du servomoteur 1 (0-180 degrés) : "))
angle2 = float(input("Entrez l'angle de rotation du servomoteur 2 (0-180 degrés) : "))

# S'assurer que les angles sont dans la plage valide
angle1 = max(0, min(angle1, 180))
angle2 = max(0, min(angle2, 180))

# Configurer la fréquence PWM
servo1_pwm.freq(servo_freq)
servo2_pwm.freq(servo_freq)

# Faire tourner les servomoteurs aux angles spécifiés
rotate_servo(servo1_pwm, angle1)
rotate_servo(servo2_pwm, angle2)

# Arrêter les PWM avant de quitter le programme
servo1_pwm.deinit()
servo2_pwm.deinit()
