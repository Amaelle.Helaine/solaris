# Créé par Baptiste.BELLEC, le 28/03/2024 en Python 3.7
import csv

# Nouvelles données à ajouter dans le fichier CSV
nouvelles_donnees = [
    {'SERVO MOTEURS 1': 'N1', 'SERVO MOTEUR 2': 'N2', },
]

# Nom du fichier CSV existant
nom_fichier = 'donnees.csv'

# Ajout des nouvelles données dans le fichier CSV
with open(nom_fichier, mode='a', newline='') as fichier_csv:
    # Création d'un objet writer
    writer = csv.DictWriter(fichier_csv, fieldnames=nouvelles_donnees[0].keys())

    # Écriture des lignes de données
    for ligne in nouvelles_donnees:
        writer.writerow(ligne)

print(f'Les nouvelles données ont été ajoutées au fichier CSV "{nom_fichier}" avec succès.')

