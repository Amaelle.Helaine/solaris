import csv
import matplotlib.pyplot as plt

# Chemin vers le fichier CSV
chemin_fichier = 'data/_capteur_data.csv'  # Remplacez par le chemin de votre fichier CSV

# Listes pour stocker les valeurs des angles
timestamps = []
angles_S1 = []
angles_S2 = []

# Lecture des données depuis le fichier CSV
with open(chemin_fichier, mode='r') as fichier_csv:
    lecteur_csv = csv.DictReader(fichier_csv)
    for ligne in lecteur_csv:
        # Ajout des valeurs des angles et des timestamps dans les listes
        timestamps.append(ligne['Timestamp'])
        angles_S1.append(float(ligne['angleV']))
        angles_S2.append(float(ligne['angleH']))

# Création du graphique
plt.figure(figsize=(10, 6))
plt.plot(timestamps, angles_S1, label='Angle S1')
plt.plot(timestamps, angles_S2, label='Angle S2')
plt.xlabel('Timestamp')
plt.ylabel('Valeur de l\'angle')
plt.title('Évolution des angles des servomoteurs')
plt.legend()
plt.xticks(rotation=45)  # Rotation des étiquettes de l'axe x pour une meilleure lisibilité
plt.tight_layout()  # Ajustement automatique des espacements
plt.grid(True)
plt.show()
