from machine import Pin, PWM
import utime, time

servo_pin_number = 18     # Définissez le numéro de la broche GPIO utilisée soit la 18
servo_pin_number1 = 19     # Définissez le numéro de la broche GPIO utilisée soit la 19

# Initialisez la broche GPIO en mode PWM
servo_pin = PWM(Pin(servo_pin_number, servo_pin_number1))

# duty_cycle [0 - 2^16-1] -> [0 - 180°]
angle = 90                                               #tourne de 90° quand on lance le programme
duty_cycle = int(angle / 360 * (2**15 - 1))#duty_cycle est égal a l'angle (90) divisé par 360 pour montrer sa proportion par rapport à un tour complet multiplier par la palge de valeurs maximale sur 16bits

for i in range(duty_cycle):      # boucle disant que le servo moteur peut tourner de i (0) à la valeur de duty_cycle  
    servo_pin.duty_u16(i)        # instruction pour le cycle de travail et definir i sur 16 bits (0- 65535)

for i in range(duty_cycle, -1, -1):   # est censé faire tourner de duty_cycle à i (0)
    servo_pin.duty_u16(i)             # ne veux pas tourner dans l'autre sens 

    
servo_pin.deinit()       # deinit sert à desinitialiser une broche utilisée autrement dit on arrêt de l'utiliser elle pourra servir pour d'autre utilisation
    