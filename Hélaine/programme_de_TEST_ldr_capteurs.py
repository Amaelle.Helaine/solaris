# déclaration des valeurs de LDR fictives
ldr_value_1 = 0.9
ldr_value_2 = 0.8
ldr_value_3 = 0.5

# définition de la fonction qui convertit la valeur de LDR en angle
def convert_ldr_to_angle(ldr_value_1, ldr_value_2, ldr_value_3):
    if ldr_value_1 > ldr_value_2 and ldr_value_1 > ldr_value_3:  # horizontal, gauche
        return 90
    elif ldr_value_1 < ldr_value_2 and ldr_value_1 < ldr_value_3:  # horizontal, droite
        return 0
    elif ldr_value_2 > ldr_value_1 and ldr_value_2 > ldr_value_3:  # vertical, haut
        return 200
    elif ldr_value_2 < ldr_value_1 and ldr_value_2 < ldr_value_3:  # vertical, bas
        return 0
    else:
        return 45  # Valeur par défaut si aucune condition n'est satisfaite

# Utilisation de la fonction avec les valeurs fictives
angle = convert_ldr_to_angle(ldr_value_1, ldr_value_2, ldr_value_3)
print(f"Sens du servo moteur : {angle} degrés")
