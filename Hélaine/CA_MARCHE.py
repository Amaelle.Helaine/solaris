"""
le progrmme de lecture des capteurs + ctionneurs
sur la Rasp PICO
envoie vers Raspberry Pi
CA MARCHE et je ne SASP APS POUR
"""   

import machine 
import time

    
def lecture_LDR():
    ldrPin1 = 26
    ldrPin2 = 27
    ldrPin3 =28
      
 
    can1 = machine.ADC(ldrPin1)
    can2 = machine.ADC(ldrPin2)
    can3 = machine.ADC(ldrPin3)
    
    N1 = can1.read_u16()
    time.sleep(0.25)
    
    N2 = can2.read_u16()
    time.sleep(0.25)
    
    N3 = can3.read_u16()
    time.sleep(0.25)
    
    return N1, N2, N3

# Activer l'interface UART avec un débit de 9600 bauds
uart = machine.UART(0, baudrate=9600)

while True:
    N1, N2, N3 = lecture_LDR()
    print("N1 =", N1 ,"N2 =", N2 ,"N3 =", N3)

    # Attendre pendant 2 secondes avant de lire à nouveau les valeurs des capteurs
    time.sleep(2)
