from flask import Flask, render_template
import serial

def ecriture_csv(filename, data):
    file = open(filename, mode='a')
    # Écrire les données dans le fichier CSV
    file.write((data)+"\n")
    file.close()

app = Flask(__name__)

# Définir le port série
port = '/dev/ttyACM1'  # Modifier selon le port USB utilisé, par exemple '/dev/ttyUSB0' ou '/dev/ttyACM0'

# Configuration de la communication série
ser = serial.Serial(port, 9600)  # Assurez-vous d'adapter la vitesse de communication (baudrate) selon votre périphérique


# Routes
@app.route('/')
def index():
     # Lire une ligne de données depuis le port série
    data = ser.readline().decode().strip()
    
    ecriture_csv('donnees.csv', data)
    
    N1, N2, N3 = data.split(',')

    return render_template('index.html', sensor_value_1=N1, sensor_value_2=N2, sensor_value_3=N3)
    

if __name__ == '__main__':
    app.run(debug=True)

