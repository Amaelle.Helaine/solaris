"""
le progrmme de lecture des données envoyées pr la Raps PICO
vleurs LDR + servo
"""

import serial

    
# Définir le port série
port = '/dev/ttyACM0'  # Modifier selon le port USB utilisé, par exemple '/dev/ttyUSB0' ou '/dev/ttyACM0'

# Configuration de la communication série
ser = serial.Serial(port, 9600)  # Assurez-vous d'adapter la vitesse de communication (baudrate) selon votre périphérique

while True:
    # Lire une ligne de données depuis le port série
    data = ser.readline().decode().strip()
 
    # Afficher les données
    print("Données reçues", data)

    