from time import sleep
from machine import Pin
from machine import PWM
import machine
import time




ldrPin1 = 26
ldrPin2 = 27
ldrPin3 = 28
incertitude = 2000
angleH = 4500
angleV = 4500



pwm = PWM(Pin(14))
pwm.freq(50)

pwm2 = PWM(Pin(15))
pwm2.freq(50)




can1 = machine.ADC(ldrPin1)
can2 = machine.ADC(ldrPin2)
can3 = machine.ADC(ldrPin3)

def setServoCycle(position):
    pwm.duty_u16(int(position))
    sleep(0.1)
     
def setServoCycle2(position):
    pwm2.duty_u16(int(position))
    sleep(0.1)




while True:
    N1 = can1.read_u16()
    N2 = can2.read_u16()
    N3 = can3.read_u16()
    print("N1 =", N1,"N2 =", N2,"N3 =",N3)
    
                                                                                                                    
    if (N1>N2 +  incertitude ):
        angleH = angleH+100
            
    if (N1<N2 + incertitude):
        angleH = angleH-100
    
    setServoCycle(angleH)
    
    if (N1<N3 +  incertitude ):
        angleV = angleV+50
            
    if (N1>N3 + incertitude):
        angleV = angleV-50
    
    setServoCycle2(angleV)  
 
        
    



     



