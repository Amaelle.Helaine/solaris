import machine
import time
from math import atan2, degrees

# Configuration des broches pour les LDR
ldrPin1 = 26
ldrPin2 = 27
ldrPin3 = 28

# Configuration des broches pour les servomoteurs angulaires
servoX_pin = 14
servoY_pin = 15

# Initialisation des ADC pour les LDR
can1 = machine.ADC(machine.Pin(ldrPin1))
can2 = machine.ADC(machine.Pin(ldrPin2))
can3 = machine.ADC(machine.Pin(ldrPin3))

# Initialisation des servomoteurs angulaires
servoX = machine.PWM(machine.Pin(servoX_pin))
servoY = machine.PWM(machine.Pin(servoY_pin))

# Configuration des limites d'angle des servomoteurs
angle_min = -90
angle_max = 90

# Fonction pour calculer l'angle à partir des valeurs des LDR
def calculate_angle(x, y):
    return degrees(atan2(x, y))

# Fonction pour ajuster les servomoteurs selon les angles calculés
def adjust_servos(angle_x, angle_y):
    # Limiter l'angle dans la plage autorisée
    angle_x = max(angle_min, min(angle_x, angle_max))  
    angle_y = max(angle_min, min(angle_y, angle_max))
    
    # Convertir l'angle en rapport cyclique PWM en nanosecondes
    duty_x = int(500000 + ((angle_x - angle_min) / (angle_max - angle_min)) * 2000000)
    duty_y = int(500000 + ((angle_y - angle_min) / (angle_max - angle_min)) * 2000000)
    
    # Définir le rapport cyclique PWM pour les servomoteurs
    servoX.duty_ns(duty_x)
    servoY.duty_ns(duty_y)

# Boucle principale
while True:
    # Lecture des valeurs des LDR
    N1 = can1.read_u16()
    time.sleep(0.25)
    N2 = can2.read_u16()
    time.sleep(0.25)
    N3 = can3.read_u16()
    time.sleep(0.25) 
    
    # Calcul des angles en fonction des valeurs des LDR
    angle_x = calculate_angle(N1 - N2, N1 - N3)
    angle_y = calculate_angle(N2 - N1, N2 - N3)
    
    # Ajustement des servomoteurs
    adjust_servos(angle_x, angle_y)
    
    print("Angle X:", angle_x)
    print("Angle Y:", angle_y)
    print("N1 =", N1, "N2 =", N2, "N3 =", N3)



