import machine
import time

def lire_capteurs():
    ldrPin1 = 26
    ldrPin2 = 27
    ldrPin3 = 28

    can1 = machine.ADC(ldrPin1)
    can2 = machine.ADC(ldrPin2)
    can3 = machine.ADC(ldrPin3)
    
    while True:
        N2 = can1.read_u16()
        time.sleep(0.25)
        
        N1 = can2.read_u16()
        time.sleep(0.25)
        
        N3 = can3.read_u16()
        time.sleep(0.25) 
        
        print("N1 =", N1, "N2 =", N2, "N3 =", N3)

# Appel de la fonction
lire_capteurs()
