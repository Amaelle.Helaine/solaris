import machine 
import time

ldrPin1 = 26
ldrPin2 = 27
ldrPin3 =28

can1 = machine.ADC(ldrPin1)
can2 = machine.ADC(ldrPin2)
can3 = machine.ADC(ldrPin3)
    
while True:
    N2 = can1.read_u16()
    time.sleep(0.25)
    
    N1 = can2.read_u16()
    time.sleep(0.25)
    
    N3 = can3.read_u16()
    time.sleep(0.25) 
    
    print("N1 =", N1,"N2 =", N2 ,"N3 =", N3)
    
#config broches servo moteurimport machine    
GPIO.setmode(GPIO.BCM)

#config broches entrée des LDR
GPIO.setup(ldrPin1, GPIO.IN)
GPIO.setup(ldrPin2, GPIO.IN)
GPIO.setup(ldrPin3, GPIO.IN)

#CONFIG broche de sortie du servo moteur
GPIO.setup(SERVO_PIN, GPIO.OUT)

#CONFIG PWM servo moteur
servo_pwm = GPIO.PWM(SERVO_PIN, 50)

try:
    servo_pwm.start(0)
    
    while True:
        #LECTURE VALEUR DES 3 LDRR
        ldr_value_1 = read_ldr_value(N1)
        ldr_value_2 = read_ldr_value(N2)
        ldr_value_3 = read_ldr_value(N3)
except:
    pass

#défintion de la fonction qui convertis la valeur de ldr 0 ou 1 en angle
def convert_ldr_to_angle(ldr_value_1, ldr_value_2, ldr_value_3):
    
    if ldr_value_1 > ldr_value_2: #horizontal
        return 90 #gauche
    else:
        return 0
    if ldr_value_1 < ldr_value_2:
        return 90 #droite
    else:
        return 0
    if ldr_value_1 > ldr_value_3: #vertical
        return 90 #haut
    else:
        return 0                      
    if ldr_value_1 < ldr_value_3:
        return 90 #bas
    else:
        return 0
    
    
    
    
    
import machine
import time

# Configuration des broches pour les servomoteurs
servo1_pin = machine.Pin(15)  # Changer le numéro de broche selon le câblage pour le servomoteur 1
servo2_pin = machine.Pin(14)  # Changer le numéro de broche selon le câblage pour le servomoteur 2

servo1_pwm = machine.PWM(servo1_pin)
servo2_pwm = machine.PWM(servo2_pin)

# Configuration des paramètres PWM pour les servomoteurs
servo_freq = 50  # Fréquence de 50 Hz pour les servomoteurs standard
angle_min = 0.5  # Largeur d'impulsion correspondant à l'angle minimum (en ms)
angle_max = 2.5  # Largeur d'impulsion correspondant à l'angle maximum (en ms)

def angle_to_duty_cycle(angle):
    # Convertir l'angle en largeur d'impulsion PWM
    duty_cycle = (angle / 180.0) * (angle_max - angle_min) + angle_min
    return int(duty_cycle / 20 * 65535)  # Conversion en valeur entre 0 et 65535

def rotate_servo(servo_pwm, angle):
    # Convertir l'angle en largeur d'impulsion PWM
    duty_cycle = angle_to_duty_cycle(angle)
    # Appliquer la largeur d'impulsion PWM au servomoteur
    servo_pwm.duty_u16(duty_cycle)
    # Laisser un peu de temps au servomoteur pour se déplacer
    time.sleep(0.1)

# Demander à l'utilisateur de saisir les angles de rotation pour les deux servomoteurs
angle1 = float(input("Entrez l'angle de rotation du servomoteur 1 (0-180 degrés) : "))
angle2 = float(input("Entrez l'angle de rotation du servomoteur 2 (0-180 degrés) : "))

# S'assurer que les angles sont dans la plage valide
angle1 = max(0, min(angle1, 180))
angle2 = max(0, min(angle2, 180))

# Configurer la fréquence PWM
servo1_pwm.freq(servo_freq)
servo2_pwm.freq(servo_freq)

# Faire tourner les servomoteurs aux angles spécifiés
rotate_servo(servo1_pwm, angle1)
rotate_servo(servo2_pwm, angle2)

# Arrêter les PWM avant de quitter le programme
servo1_pwm.deinit()
servo2_pwm.deinit()
    
    

        
  
