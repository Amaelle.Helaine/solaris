import machine 
import time

ldrPin1 = 26
ldrPin2 = 27
ldrPin3 =28

can1 = machine.ADC(ldrPin1)
can2 = machine.ADC(ldrPin2)
can3 = machine.ADC(ldrPin3)
    
while True:
    N1 = can1.read_u16()
    time.sleep(0.25)
    
    N2 = can2.read_u16()
    time.sleep(0.25)
    
    N3 = can3.read_u16()
    time.sleep(0.25) 
    
    print("N1 =", N1,"N2 =", N2 ,"N3 =", N3)
    
#config broches servo moteurimport machine    
GPIO.setmode(GPIO.BCM)

#config broches entrée des LDR
GPIO.setup(ldrPin1, GPIO.IN)
GPIO.setup(ldrPin2, GPIO.IN)
GPIO.setup(ldrPin3, GPIO.IN)

#CONFIG broche de sortie du servo moteur
GPIO.setup(SERVO_PIN, GPIO.OUT)

#CONFIG PWM servo moteur
servo_pwm = GPIO.PWM(SERVO_PIN, 50)

try:
    servo_pwm.start(0)
    
    while True:
        #LECTURE VALEUR DES 3 LDRR
        ldr_value_1 = read_ldr_value(N1)
        ldr_value_2 = read_ldr_value(N2)
        ldr_value_3 = read_ldr_value(N3)
except:
    pass

#défintion de la fonction qui convertis la valeur de ldr 0 ou 1 en angle
def convert_ldr_to_angle(ldr_value_1, ldr_value_2, ldr_value_3):
    
    if ldr_value_1 > ldr_value_2: #horizontal
        return 90 #gauche
    else:
        return 0
    if ldr_value_1 < ldr_value_2:
        return 90 #droite
    else:
        return 0
    if ldr_value_1 > ldr_value_3: #vertical
        return 90 #haut
    else:
        return 0                      
    if ldr_value_1 < ldr_value_3:
        return 90 #bas
    else:
        return 0
        
  
